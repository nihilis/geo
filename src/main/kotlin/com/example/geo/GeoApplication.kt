package com.example.geo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class])
class GeoApplication

fun main(args: Array<String>) {
    runApplication<GeoApplication>(*args)
}

fun cleanToInt(string: String): Int {
    return string.replace(("[^\\d.]").toRegex(), "").toInt()
}

fun strArray(string: String, delimiter: String = ","): List<Int> {
    return string.split(delimiter).mapNotNull { v -> v.trim().toIntOrNull() }.toList()
}

fun parseMeasureParams(params: Map<String, String>): Map<Int, List<Int>> {
    return params.filter {
        it.key.startsWith("measure[")
    }.map {
        cleanToInt(it.key) to strArray(it.value)
    }.toMap()
}


data class Indicator(val id: Int, val title: String)
data class Measure(val id: Int, val title: String, val multiple: Boolean, val values: List<MeasureValue>? = null)
data class MeasureValue(val id: Int, val title: String, val measureId: Int)
data class Indicator2Measure(val indicatorId: Int, val measureId: Int, val numInFactTable: Int)
data class Territory(val id: Int, val title: String)
data class Fact(val indicatorId: Int, val measure1: Int?, val measure2: Int?, val measure3: Int?, val territoryId: Int, val value: Double)

data class IndicatorData(val indicator: Indicator, val measures: List<Measure>)
data class FactData(val territory: Territory?, var sum: Double)

val t_indicators = listOf(
        Indicator(1, "Количество проживающих"),
        Indicator(2, "Количество ночующих"),
        Indicator(3, "Количество зданий"),
)
val t_measures = listOf(
        Measure(1, "Пол", false),
        Measure(2, "Возраст", true),
        Measure(3, "Высотность", true),
)
val t_measure_values = listOf(
        MeasureValue(1, "Мужчины", 1),
        MeasureValue(2, "Женщины", 1),

        MeasureValue(3, "До 18 лет", 2),
        MeasureValue(4, "18-30 лет", 2),
        MeasureValue(5, "Более 30 лет", 2),

        MeasureValue(6, "1 этаж", 3),
        MeasureValue(7, "2-9 этажей", 3),
        MeasureValue(8, "10 и более этажей", 3),
)
val t_indicator2measure = listOf(
        Indicator2Measure(1, 1, 1),
        Indicator2Measure(1, 2, 2),

        Indicator2Measure(2, 2, 1),

        Indicator2Measure(3, 3, 1),
)
val t_territory = listOf(
        Territory(1, "Первый район"),
        Territory(2, "Второй район"),
        Territory(3, "Третий район"),
)
val t_facts = listOf(
        // # Первый район. Количество проживающих
        // 15 муж, 17 жен в возрасте до 18
        Fact(1, 1, 3, null, 1, 15.0),
        Fact(1, 2, 3, null, 1, 17.0),
        // 25 муж, 19 жен в возрасте 18-30
        Fact(1, 1, 4, null, 1, 25.0),
        Fact(1, 2, 4, null, 1, 19.0),
        // 5 муж, 11 жен в возрасте более 30
        Fact(1, 1, 5, null, 1, 5.0),
        Fact(1, 2, 5, null, 1, 11.0),

        // # Второй район. Количество проживающих
        // 5 муж, 13 жен в возрасте до 18
        Fact(1, 1, 3, null, 2, 5.0),
        Fact(1, 2, 3, null, 2, 13.0),
        // 50 муж, 31 жен в возрасте 18-30
        Fact(1, 1, 4, null, 2, 50.0),
        Fact(1, 2, 4, null, 2, 31.0),
        // 15 муж, 19 жен в возрасте более 30
        Fact(1, 1, 5, null, 2, 15.0),
        Fact(1, 2, 5, null, 2, 19.0),

        // # Третий район. Количество проживающих
        // 41 муж, 49 жен в возрасте до 18
        Fact(1, 1, 3, null, 3, 41.0),
        Fact(1, 2, 3, null, 3, 49.0),
        // 45 муж, 57 жен в возрасте 18-30
        Fact(1, 1, 4, null, 3, 45.0),
        Fact(1, 2, 4, null, 3, 57.0),
        // 27 муж, 33 жен в возрасте более 30
        Fact(1, 1, 5, null, 3, 27.0),
        Fact(1, 2, 5, null, 3, 33.0),

        // # Первый район. Количество ночующих
        // 30 в возрасте до 18
        Fact(2, 3, null, null, 1, 30.0),
        // 42 в возрасте 18-30
        Fact(2, 4, null, null, 1, 42.0),
        // 12 в возрасте более 30
        Fact(2, 5, null, null, 1, 12.0),

        // # Второй район. Количество ночующих
        // 17 в возрасте до 18
        Fact(2, 3, null, null, 2, 17.0),
        // 80 в возрасте 18-30
        Fact(2, 4, null, null, 2, 80.0),
        // 33 в возрасте более 30
        Fact(2, 5, null, null, 2, 33.0),

        // # Третий район. Количество ночующих
        // 88 в возрасте до 18
        Fact(2, 3, null, null, 3, 88.0),
        // 101 в возрасте 18-30
        Fact(2, 4, null, null, 3, 101.0),
        // 53 в возрасте более 30
        Fact(2, 5, null, null, 3, 53.0),

        // # Первый район. Количество зданий
        // 49 зданий, 1 этаж
        Fact(3, 6, null, null, 1, 49.0),
        // 18 зданий, 2-9 этажей
        Fact(3, 7, null, null, 1, 18.0),
        // 3 здания, 10 и более этажей
        Fact(3, 8, null, null, 1, 3.0),

        // # Второй район. Количество зданий
        // 19 зданий, 1 этаж
        Fact(3, 6, null, null, 2, 19.0),
        // 5 зданий, 2-9 этажей
        Fact(3, 7, null, null, 2, 5.0),
        // 0 зданий, 10 и более этажей
        Fact(3, 8, null, null, 2, 0.0),

        // # Третий район. Количество зданий
        // 14 зданий, 1 этаж
        Fact(3, 6, null, null, 3, 14.0),
        // 28 зданий, 2-9 этажей
        Fact(3, 7, null, null, 3, 28.0),
        // 9 зданий, 10 и более этажей
        Fact(3, 8, null, null, 3, 9.0),
)

@RestController
class IndicatorController {
    @GetMapping("/api/indicators")
    fun index() = t_indicators.map { indicator ->
        run {
            val measureIDs = t_indicator2measure.filter { it.indicatorId == indicator.id }.map { it.measureId }
            val measures = t_measures.filter { measureIDs.contains(it.id) }.map {
                it.copy(values = t_measure_values.filter { value -> value.measureId == it.id })
            }

            IndicatorData(indicator, measures)
        }
    }
}


@RestController
class FactsController {
    @GetMapping("/api/facts")
    fun index(@RequestParam("indicator") indicatorId: Int, @RequestParam params: Map<String, String>): List<FactData> {
        val measureFilters = parseMeasureParams(params)
        var facts = t_facts.filter { it.indicatorId == indicatorId }
        val results = mutableMapOf<Int, FactData>()

        measureFilters.forEach { (measureId, valueIDs) ->
            run {
                val measureConfig = t_indicator2measure.firstOrNull { i2m ->
                    i2m.measureId == measureId && i2m.indicatorId == indicatorId
                }

                measureConfig?.let {
                    facts = facts.filter {
                        valueIDs.contains(when (measureConfig.numInFactTable) {
                            1 -> it.measure1
                            2 -> it.measure2
                            3 -> it.measure3
                            else -> -1
                        })
                    }
                }
            }
        }

        facts.forEach { fact ->
            run {
                val factData = results.getOrPut(fact.territoryId) {
                    FactData(t_territory.firstOrNull { it.id == fact.territoryId }, 0.0)
                }
                factData.sum += fact.value
            }
        }

        return results.values.toList()
    }
}
